const assert = require('chai').assert;
const app = require('../index').main;

activeModulesResult = app.activeModules;
getCustomModuleNumberResult = app.getCustomModuleNumber();

describe('App', () => {
	describe('activeModules', () => {
		it('activeModules should be type array', () => {
			assert.isArray(activeModulesResult);
		});
		it('activeModules has the same members', () => {
			const testArray = [
				{ name: 'module 1' },
				{ name: 'module 2' },
				{ name: 'module 11' },
				{ name: 'module 3' },
				{ name: 'module 10' },
			];
			assert.includeDeepMembers(activeModulesResult, testArray , 'same members');
		});
	});
	describe('getCustomModuleNumber', () => {
		it('getCustomModuleNumber should be type number', () => {
			assert.isNumber(getCustomModuleNumberResult);
		});
		it('getCustomModuleNumber should return 11', () =>  {
			assert.equal(getCustomModuleNumberResult, 11);
		});
	});
})
