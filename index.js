'use strict'
const activeModules = [
	{ name: 'module 1' },
	{ name: 'module 2' },
	{ name: 'module 11' },
	{ name: 'module 3' },
	{ name: 'module 10' },
];
const getCustomModuleNumber = _ =>
	Math.max(...activeModules.map( n =>
		n.name.match("\\d+")
));
exports.main = {
	activeModules: activeModules,
	getCustomModuleNumber: getCustomModuleNumber
}
